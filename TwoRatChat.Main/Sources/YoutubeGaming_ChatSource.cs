﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Web;
using System.Windows.Threading;
using System.Xml.Linq;
using TwoRatChat.Controls;

namespace TwoRatChat.Main.Sources {
    public class YoutubeGaming_ChatSource: TwoRatChat.Model.ChatSource {
        /// <summary>
        /// Тут я его должен убрать из исходников, но в общем кто знает
        /// </summary>
        string YOUTUBE_APIKEY = "AIzaSyDWmvsh7PCyrV614EnBsUxzAB7oZkb7iZQ";

        const string API_getViewerCount = "https://www.googleapis.com/youtube/v3/videos?part=liveStreamingDetails&id={0}&key={1}";
        const string API_getMessages = "https://www.googleapis.com/youtube/v3/liveChat/messages?liveChatId={0}&part=snippet%2CauthorDetails&key={1}";

        class YoutubeMessage {
            public DateTime publishedAt;
            public string displayMessage;
        }

        class YoutubeAuthor {
            public string displayName;
            public bool isVerified;
            public bool isChatOwner;
            public bool isChatSponsor;
            public bool isChatModerator;
            public string profileImageUrl;
        }

        class YoutubeMessageContainer {
            public string kind;
            public string etag;
            public string id;
            public YoutubeMessage snippet;
            public YoutubeAuthor authorDetails;

            [JsonIgnore]
            public int NeedToDelete;
        }

        class YoutubeLiveMessages {
            public string nextPageToken;
            public int pollingIntervalMillis;
            public YoutubeMessageContainer[] items;
        }

        string _liveChatId;
       // Timer _retriveTimer;
        //List<YoutubeMessageContainer> _cache;
        string _id;
        DateTime _last;
        bool _showComments = false;

        public override string Id { get { return "youtube"; } }

        public YoutubeGaming_ChatSource( Dispatcher dispatcher )
            : base(dispatcher) {
        }

        public override void UpdateViewerCount() {
        }

        private class MyWebClient : WebClient {
            protected override WebRequest GetWebRequest( Uri uri ) {
                WebRequest w = base.GetWebRequest(uri);
                w.Timeout = 1000;
                return w;
            }
        }

        //YoutubeMessageContainer getById( string id ) {
        //    for (int j = 0; j < _cache.Count; ++j)
        //        if (_cache[j].id == id)
        //            return _cache[j];
        //    return null;
        //}

        Uri[] getBages( YoutubeMessageContainer b ) {
            if( Properties.Settings.Default.youtube_ShowIcons ) {
                return new System.Uri[] { new Uri( b.authorDetails.profileImageUrl, UriKind.Absolute ) };
            }
            return new System.Uri[0];
        }

        private void readMessages( string activeLiveChatId ) {
            
        }

        //eWRei_9cEO8
        public override void Create( string streamerUri, string id ) {
            this.Label = this._id = id;
            this.Uri = this.SetKeywords( streamerUri, false );
            this.Tooltip = "youtube";

            _showComments = Properties.Settings.Default.Chat_LoadHistory;

            //_cache = new List<YoutubeMessageContainer>();
            _youtubeChannelId = "";
            _last = DateTime.Now.AddDays( -10 );
            _chatThread = new System.Threading.Thread( chatThreadFunc );
            _chatThread.Start();
        }

        bool _abortRequested = false;
        System.Threading.Thread _chatThread;

        dynamic api( string url ) {
            MyWebClient mwc = new MyWebClient();
            try {
                byte[] data = mwc.DownloadData( url );
                return Newtonsoft.Json.JsonConvert.DeserializeObject( Encoding.UTF8.GetString( data ) );
            } catch( Exception er ) {
                return null;
            }
        }

        string _youtubeChannelId;
        string _youtubeLiveVideoId;
        MyWebClient _chatLoader = new MyWebClient();

        void chatThreadFunc() {
            bool needErrorSleep = false;
            Header = "Loading 0%";
            int sleepMs = 3000;

            while ( !_abortRequested ) {
                if ( string.IsNullOrEmpty( _youtubeChannelId ) ) {
                    dynamic channelInfo = api( string.Format( "https://www.googleapis.com/youtube/v3/channels?part=id&forUsername={0}&key={1}",
                        HttpUtility.UrlEncode( this.Uri ),
                        YOUTUBE_APIKEY ) );

                    if ( channelInfo == null ) {
                        needErrorSleep = true;
                        Header = "Net error";
                    } else
                    if ( channelInfo.items.Count == 0 ) {
                        needErrorSleep = true;
                        Header = "Not found";
                    } else {
                        needErrorSleep = false;
                        Header = "Loading 30%";
                        _youtubeChannelId = (string)channelInfo.items[0].id;
                    }

                    if ( string.IsNullOrEmpty( _youtubeChannelId ) ) {
                        _youtubeChannelId = this.Uri;
                    }
                }

                if ( !string.IsNullOrEmpty( _youtubeChannelId ) ) {

                    if ( string.IsNullOrEmpty( _youtubeLiveVideoId ) ) {
                        dynamic liveVideo = api( string.Format( "https://www.googleapis.com/youtube/v3/search?part=id&channelId={0}&eventType=live&type=video&key={1}&rnd={2}",
                            _youtubeChannelId,
                            YOUTUBE_APIKEY, DateTime.Now.ToBinary() ) );

                        if ( liveVideo == null ) {
                            Header = "Net error";
                            _liveChatId = "";
                        } else
                        if ( liveVideo.items.Count == 0 ) {
                            Header = "No live";
                            _liveChatId = "";
                            _youtubeChannelId = "";
                        } else {
                            Header = "Live";
                            _youtubeLiveVideoId = liveVideo.items[0].id.videoId;
                        }
                    }
                }

                if ( !string.IsNullOrEmpty( _youtubeLiveVideoId ) ) {
                    dynamic d = api( string.Format( API_getViewerCount,
                        _youtubeLiveVideoId,
                        YOUTUBE_APIKEY ) );
                    if ( d != null ) {
                        if ( d.items[0].liveStreamingDetails.concurrentViewers == null ) {
                            this.Header = "Offline?";
                            _youtubeLiveVideoId = "";
                            _liveChatId = "";
                        } else {
                            int h = (int)d.items[0].liveStreamingDetails.concurrentViewers;
                            this.ViewersCount = h;
                            this.Header = h.ToString();
                            _liveChatId = (string)d.items[0].liveStreamingDetails.activeLiveChatId;
                        }
                    } else {
                        this.Header = "Net?";
                        _youtubeLiveVideoId = "";
                        _liveChatId = "";
                    }
                }

                if( !string.IsNullOrEmpty( _liveChatId ) ) {
                    try {
                        byte[] data = _chatLoader.DownloadData( string.Format( API_getMessages, _liveChatId, YOUTUBE_APIKEY ) );

                        string x = Encoding.UTF8.GetString( data );

                        YoutubeLiveMessages d = Newtonsoft.Json.JsonConvert.DeserializeObject<YoutubeLiveMessages>( x );
                        Status = true;

                        sleepMs = d.pollingIntervalMillis;

                        List<YoutubeMessageContainer> NewMessage = new List<YoutubeMessageContainer>();
                        ////////////
                        foreach ( var m in from b in d.items
                                           orderby b.snippet.publishedAt
                                           select b ) {
                            if ( m.snippet.publishedAt > _last ) {
                                NewMessage.Add( m );
                                _last = m.snippet.publishedAt;
                            }
                        }

                        if ( _showComments ) {
                            if ( NewMessage.Count > 0 ) {
                                newMessagesArrived( from b in NewMessage
                                                    orderby b.snippet.publishedAt
                                                    select new TwoRatChat.Model.ChatMessage( getBages( b ) ) {
                                                        Date = DateTime.Now,
                                                        Name = b.authorDetails.displayName,
                                                        Text = HttpUtility.HtmlDecode( b.snippet.displayMessage ),
                                                        Source = this,
                                                        Id = _id,
                                                        ToMe = this.ContainKeywords( b.authorDetails.displayName ),

                                                        //Form = 0
                                                    } );
                            }

                        }

                        _showComments = true;

                    } catch {
                        Status = false;
                        Header = "ERR??";
                        _liveChatId = "";
                        _youtubeLiveVideoId = "";
                    }

                }


                Thread.Sleep( sleepMs );
            }
        }

        public override void Destroy() {
            _abortRequested = true;
            _chatThread.Abort();
        }
    }
}
