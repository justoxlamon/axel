﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace TwoRatChat.Model {
    public abstract class ChatSource : NotifyPropertyChanged {
        public enum FatalErrorCodeEnum {
            Success = 0,

            ChannelNotFound = 1,
            ParsingError = 2
        }

        string _header = string.Empty;
        string _tooltip = string.Empty;
        int? _ViewersCount = null;
        bool _status = false;

        public string Header {
            get { return _header; }
            set {
                if( _header != value ){
                    _header = value;
                    raisePropertyChanged("Header");
                }
            }
        }

        public string Tooltip {
            get { return _tooltip; }
            set {
                if (_tooltip != value) {
                    _tooltip = value;
                    raisePropertyChanged( "Tooltip" );
                }
            }
        }

        public abstract string Id { get; }
        public int SystemId { get; set; }
        public bool Status {
            get { return _status; }
            set {
                if (_status != value) {
                    _status = value;
                    raisePropertyChanged( "Status" );
                }
            }
        }
  
        public string Uri { get; protected set; }
        public string Label { get; protected set; }

        HashSet<string> _keyWords = new HashSet<string>();

        public bool ContainKeywords( string text ) {
            string t = text.ToLower();
            foreach (var x in _keyWords)
                if (t.Contains( x ))
                    return true;
            return false;
        }

        public void AddKeyword( string text ) {
            _keyWords.Add( text.Trim().ToLower() );
        }

        public string SetKeywords( string text, bool toLower = true ) {
            if ( toLower ) {
                var keywords = from b in text.Split( new string[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries )
                               select b.Trim().ToLower();
                _keyWords = new HashSet<string>( keywords );
                return keywords.FirstOrDefault() ?? string.Empty;
            } else {
                var keywords = from b in text.Split( new string[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries )
                               select b.Trim();
                _keyWords = new HashSet<string>( keywords );
                return keywords.FirstOrDefault() ?? string.Empty;
            }
        }

        public ChatSource( Dispatcher dispatcher )
            : base(dispatcher) {
        }

        public int? ViewersCount {
            get { return _ViewersCount; }
            protected set {
                if (_ViewersCount != value) {
                    _ViewersCount = value;
                    raisePropertyChanged("ViewersCount");
                }
            }
        }

        public delegate void OnNewMessagesDelegate( IEnumerable<TwoRatChat.Model.ChatMessage> messages );
        public delegate void OnNewCommandsDelegate( IEnumerable<TwoRatChat.Model.ChatCommand> cmds );
        public delegate void OnFatalErrorDelegate( ChatSource sender, FatalErrorCodeEnum code );
        public delegate void OnRemoveUserMessagesDelegate(ChatSource sender, string userName);
        public delegate void OnNewFollowerDelegate(ChatSource sender, string userName);

        public event OnNewMessagesDelegate OnNewMessages;
        public event OnNewCommandsDelegate OnNewCommands;
        public event OnFatalErrorDelegate OnFatalError;
        public event OnRemoveUserMessagesDelegate OnRemoveUserMessages;
        public event OnNewFollowerDelegate OnNewFollower;

        protected void fireOnNewFollower(string userName) {
            if ( OnNewFollower != null )
                OnNewFollower( this, userName );
        }

        protected void fireOnRemoveUserMessages(string userName) {
            if ( OnRemoveUserMessages != null )
                OnRemoveUserMessages( this, userName );
        }

        protected void fireOnFatalError(FatalErrorCodeEnum code) {
            if ( OnFatalError != null )
                OnFatalError( this, code );
        }

        protected void newMessagesArrived( IEnumerable<TwoRatChat.Model.ChatMessage> messages ) {
            if (OnNewMessages != null)
                OnNewMessages(messages);
        }

        protected void newCommandsArrived( IEnumerable<TwoRatChat.Model.ChatCommand> cmds ) {
            if (OnNewCommands != null)
                OnNewCommands( cmds );
        }

        public virtual void ReloadChatCommand() {
        }

        public abstract void Create( string streamerUri, string id );
        public abstract void Destroy();
        public abstract void UpdateViewerCount();
    }
}
