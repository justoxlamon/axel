﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwoRatChat.Model {
    public class ChatCommand {
        public ChatCommand( string nickName ) {
            this.NickName = nickName;
        }

        public readonly string NickName;
    }
}
